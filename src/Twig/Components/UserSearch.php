<?php
// src/Twig/Components/ProductSearch.php
namespace App\Twig\Components;

use App\Repository\UserRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class UserSearch
{
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public string $query = '';

    public function __construct(private UserRepository $userRepository)
    {
    }

    public function getUsers(): array
    {
        // example method that returns an array of Products
        return $this->userRepository->findByQuery($this->query);
        
        //$this->userRepository->search($this->query);
    }
}